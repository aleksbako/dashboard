# Dashboard

## Homescreen
This is the landing page where it shows how to get started with the web application.
![homescreen.png](./resources/homePage.PNG).


## Login page
Login page is where the user signs up using google and recieves an access token which is then used by the server to get the google calendar data from google api.
![loginpage.png ](./resources/login-img.PNG).
## Agenda page
Once the user has been signed in, the Agenda page will provice a breakdown of all the upcoming events within the next 12 months, all sorted by month basis.
![agendapage.png ](./resources/dashboard-img.PNG).


