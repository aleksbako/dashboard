const express = require('express')
 const app = express()
 const {google} = require('googleapis')
 const dotenv = require('dotenv').config({
  path: "../.env"
 })


 const cors = require('cors');
 app.use(cors({
     origin: 'http://localhost:3000'
 }));




 app.get("/" ,(req, res) => {
    res.send("homepage")
 })

 app.get("/dashboard", async (req, res) => {
  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
  let map = new Map();
  months.map((item) => map.set(item,new Array()))

   const date = new Date()
    let token = req.headers.access_token
  
    let response = []
  
   if(token !== 'undefined'){
try{
   const oAuth2Client = new google.auth.OAuth2(
    dotenv.parsed.CLIENT_ID,
    dotenv.parsed.CLIENT_SECRET
)

oAuth2Client.setCredentials({refresh_token: token})
const calendar = google.calendar({version: 'v3', auth: oAuth2Client})
   const fetch = await calendar.events.list({
      // Calendar identifier. To retrieve calendar IDs call the calendarList.list method. If you want to access the primary calendar of the currently logged in user, use the "primary" keyword.
      calendarId: 'primary',
      timeMin: date,
      timeMax: new Date(date.getFullYear()+1, date.getMonth()-1, date.getDate()),
      singleEvents: true,
      orderBy: "startTime"
    });
 
    let items = fetch.data.items;

      items.map(item => {
        if(item.start.date != undefined){
          let valStart = item.start.date.split("-")
          let start = new Date(valStart[0],valStart[1]-1, valStart[2]-1)
          let valEnd = item.end.date.split("-")
         let end = new Date(valEnd[0],valEnd[1]-1, valEnd[2]-1)
       
        map.get(months[valStart[1]-1]).push({summary: item.summary, start : start.toLocaleString('en-GB') , end: end.toLocaleString('en-GB'), month: parseInt(valStart[1])-1, year: start.getFullYear()})
     
        }else{

        let month =  new Date(item.start.dateTime).getMonth() 
        map.get(months[month]).push({summary: item.summary, start : new Date(item.start.dateTime).toLocaleString('en-GB') , end: new Date(item.end.dateTime).toLocaleString('en-GB'), month: month, year: new Date(item.start.dateTime).getFullYear()})
      
      }
      })
    }catch{
      console.log('error token error')
     return
    }
      response.push({data: Array.from(map)})
    }
   console.log(map)
    res.send({data: Array.from(map)})
 })
 
 app.listen(5000, () => {
    console.log("listening to port 5000")
 }) 

 
