import React from 'react'
import {Button,Form, Container} from 'react-bootstrap';

import NavigationBar from '../NavigationBar';


export default function Register(){
    
    return (<div >
        <NavigationBar />
        <Container className='content'>
          <h1>Register form</h1>
          <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" />
        <Form.Text className="text-muted">

        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" />
      </Form.Group>
      <Button> Submit</Button>
    </Form></Container> </div>)
}