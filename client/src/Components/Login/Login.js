import React, { useEffect, useState } from 'react'
import { Button, Container, Figure, Row } from 'react-bootstrap'
import NavigationBar from '../NavigationBar'

import image from './img/login.png'

export default function Login(props){
    const [login, setLogin] = useState(false)
    const [loaded, setLoaded] = useState(false)
    const [google, setGoogle] = useState({})
   
    /**
 * Get the value of a cookie
 * Source: https://gist.github.com/wpsmith/6cf23551dd140fb72ae7
 * @param  {String} name  The name of the cookie
 * @return {String}       The cookie value
 */
     function getCookie (name) {
        let value = `; ${document.cookie}`;
        let parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }

    useEffect(()=> {
    setLoaded(true)
    setGoogle(window.google)
  },[window.google])
  

  useEffect( ()=> {
    if(loaded){
    if(getCookie('token') !== undefined){
        setLogin(true)
    }
    const google = window.google
   google.accounts.id.initialize({
    client_id: "395858403379-bkid917fjh3dk7c4vbkc2afoqv3p8272.apps.googleusercontent.com",
    callback: handleCallback
   })
    }

},[loaded])

  if(!loaded){
    return <div>
        <Container>
            <h1> Loading</h1>
        </Container>
    </div>
  }
  
    const client = google.accounts.oauth2.initTokenClient({
        
        client_id: process.env.REACT_APP_CLIENT_ID,
        scope: 'https://www.googleapis.com/auth/calendar.readonly',
        callback: (response) => {
            document.cookie = `token=${response.access_token};max-age=3599`; 
          
        },
      });

    const handleCallback = (response) => {
      
    }
   



    return <div>
        <NavigationBar />
        <div className="content grid-login ">
          <div></div>
          <div><h1>Login page</h1></div>
          <div></div>
          <div></div>
          <div className='grid-login-mid'>
            <div style={{'padding-left': '3rem'}}>
            <Figure>
      <Figure.Image
        width={300}
        height={300}
        alt="image"
        src={image}
      />
      </Figure>
            </div>
                {!login &&
                <div >
                <div  id="LoginDiv" />
                    <Button style={{'margin-left': '10rem'}} onClick={() => {setLogin(true);client.requestAccessToken()}}> Login</Button>
                    </div>
                }

                {login &&
                <div >
                
                    <Button style={{'margin-left': '10rem'}} onClick={() => {setLogin(false);document.cookie = `token=value;max-age=0`}}> logout</Button>
                    </div>}
                    </div>
        </div>
    </div>

    
}