import {React,useEffect, useState} from "react"
import {Navigate} from "react-router-dom"
import {Container, Row,Spinner, Col} from 'react-bootstrap'
import NavigationBar from "../NavigationBar";
import AgendaList from "./AgendaList"


export default function Agenda(){



    /**
 * Get the value of a cookie
 * Source: https://gist.github.com/wpsmith/6cf23551dd140fb72ae7
 * @param  {String} name  The name of the cookie
 * @return {String}       The cookie value
 */
function getCookie (name) {
	let value = `; ${document.cookie}`;
	let parts = value.split(`; ${name}=`);
	if (parts.length === 2) return parts.pop().split(';').shift();
}
    
    const [data, setData] = useState({});
    const [loaded, setLoaded] = useState(false)
    const handleData = (val) => setData(val) 
    useEffect( () => {
        async function fetchData(){
        
                const res = await fetch("http://localhost:5000/dashboard",{
                    headers: {
                        access_token: getCookie("token")
                    }
                }).then(r => r.json())
                handleData(res)
                setLoaded(true)
        }
        if(JSON.stringify(data) === '{}'){
            fetchData()
        }
        
      },[]);
      

      if(getCookie("token") == undefined){
        return <Navigate to="/login" />
      }

if(loaded ){
            //add so it generates multiple lists for each month or week
    
        let temp = data.data.sort((a,b) => { 
            if(a[1][0] != undefined && b[1][0] != undefined){
                if(a[1][0].year > b[1][0].year) {
                    return 1 
                } else {
                    return -1
                }
            }
        })

      const list =  temp.map((item) => <AgendaList id={item[0]} data={item[1]}/>)

    return (<div>
        <NavigationBar />
        
        <div className="content" >
        
        <h1 style={{'text-align': 'center'}}> Agenda page</h1>
    
            {list}
            
    </div>
    </div>)
}else{
  
return (<Container>
    <h1> Please wait a moment</h1>
    <Row>
        <h1>
    <Spinner animation="border" />
    </h1>
    </Row>
</Container>)
}
}