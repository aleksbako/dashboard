import {React, useState} from 'react'
import AgendaItem from './AgendaItem'
import {ListGroup, Collapse, Container, Button} from 'react-bootstrap'
export default function AgendaList(props){

  const [open,setOpen] = useState(false)
  const handleOpen = () => setOpen(!open)
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
    const list = props.data.map((item, i) => <AgendaItem key={i} name={item.summary} start={item.start} end={item.end} month={months[item.month]} year={item.year}/>)
    return ( 
    <div className='AgendaList'>
    
      <div onClick={handleOpen} > <h4 style={{'font-weight': 'bold',
  'text-transform': 'uppercase'}}>
    {props.id}
    </h4></div>
      <Collapse in={open}>
    <ul >
        {list}
      </ul>
      </Collapse>
    
      </div>)
}