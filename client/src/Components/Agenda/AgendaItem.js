import {React,useState} from 'react'
import {Container, Row, Col, ListGroup, Collapse} from 'react-bootstrap'
export default function AgendaItem(props){
    const [open,setOpen] = useState(false)
    const handleOpen = () => setOpen(!open)
    return ( 
        <div className='grid-agenda-item'>
     
            
        <li onClick={handleOpen} className='position-static grid-agenda-item-child' >
          
          <div className='AgendaItem'>
          
        <div  className='dateItem' style={{'color': '#ececec'}}>
            
            {props.start.split("/")[0]}
        
        </div>
        <div style={{'color': '#ececec', 'font-style': 'italic'}} >
          {props.month}
        </div>
        </div>
       <div className='summary-item'>{props.name}</div>
       
       
       </li>
        
       
        <Collapse className="collapse-adjust"in={open}>
            <div>
           { <p>Start time: {props.start.split(", ")[1]}</p>  } 
           { <p>End time: {props.end.split(", ")[1]}</p> } 
           { <p>year: {props.year}</p> } 
            </div>
        </Collapse>
        
        
        </div>
      )
}