import {React} from "react"
import {Container, Figure, Row} from "react-bootstrap"
import NavigationBar from "./NavigationBar"
import {BiLogIn} from 'react-icons/bi'
import {BsFillCalendar2WeekFill} from 'react-icons/bs'
import login from './resources/img/login-img.PNG'
import dashboard from './resources/img/dashboard-img.PNG'
function Main(props){
    return (
    <div>
        <NavigationBar />
        <div className="content">
        <div className="content grid-main">
           
            <div className="grid-main-title">
        <h1 className="mainTitle">Event handling made easy</h1>
        <div><p className="subTitle">Dashboard-app helps you follow your google calendar without any disturbances, Simplifying the event list and only listing what is most important to you</p></div>
        </div>
        
    </div>
    </div>
    <div style={{'margin-top':'5rem'}}>
    <div style={{'margin-left':'5rem'}}>
            <h2 style={{'font-weight': 'bold','letter-spacing': '0.1ch' }}>Get Started:</h2>
        </div>
        <div  className="grid-main-desc">
            <div className="gird-main-desc-item"style={{'justify-self': 'center', 'margin-bottom': '30rem', 'border-top-left-radius': '30px', 'border-top-right-radius': '30px', 'border-bottom-right-radius': '30px'}}>
            <h3> <BiLogIn /> Login</h3>
            <p>First login into your account through google to gain access to your agenda dashboard</p>
            </div>
            <Figure style={{'justify-self': 'left','padding-right': '2rem'}}>
      <Figure.Image
        width={700}
        height={300}
        alt="Login-Img"
        src={login}
        rounded={true}
      />
      </Figure >
      <Figure style={{'justify-self': 'center','padding-left': '2rem'} }>
      <Figure.Image
        width={700}
        height={300}
        alt="Dashboard-Img"
        src={dashboard}
        rounded={true}
      />
      </Figure>
        <div className="gird-main-desc-item"style={ {'margin-right':'10rem','margin-bottom': '25rem', 'border-top-left-radius': '30px', 'border-top-right-radius': '30px', 'border-bottom-left-radius': '30px'}}>
            
      <h3>  <BsFillCalendar2WeekFill /> Dashboard</h3>
      <p>Once you have logged into your account, navigate yourself to the dashboard page. Once there then you may view begin breakdown of your activities</p>
      </div>
     
        </div>
    </div>
    </div>)
}
export default Main