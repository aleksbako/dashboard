import React from 'react'
import { BsFillCalendar2WeekFill } from 'react-icons/bs';
import {AiFillHome} from 'react-icons/ai'
export default function NavigationBar(props){

  /**
 * Get the value of a cookie
 * Source: https://gist.github.com/wpsmith/6cf23551dd140fb72ae7
 * @param  {String} name  The name of the cookie
 * @return {String}       The cookie value
 */
   function getCookie (name) {
    let value = `; ${document.cookie}`;
    let parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

    return (
        <nav className="navbar"  >
         <ul className="navbar-nav">
          <li className='logo'>

            <h1>Dashboard-app</h1>
          
          </li>
          
       
          <li className="nav-item"><a href="/" className="nav-link">
         <AiFillHome className='link-text' size="30"> </AiFillHome>
         
          </a>
          </li>
          <li className="nav-item">
            <a href="/agenda" className="nav-link">
           <BsFillCalendar2WeekFill size="30"/>
          </a>
          </li >
          
          <li className="nav-item"><a href="/login" className="nav-link">
         { getCookie('token') == undefined ? <p style={{'font-size': '1.2rem'}}>Login</p> : <p style={{'font-size': '1.2rem'}}>Logout</p>}
          </a>
          </li>
        
        </ul>

   
        </nav>
      );
}