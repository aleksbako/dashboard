
import Main from './Components/Main'
import Agenda from './Components/Agenda/Agenda'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import './App.css';
import Register from './Components/Register/Register';
import Login from './Components/Login/Login';

function App() {


  
  return (
    <div className="App">
      <BrowserRouter>
        <Routes> 
        <Route path="/" element={<Main />} />
        <Route path="/agenda" element={<Agenda />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
